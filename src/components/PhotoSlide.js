import React, { Component } from "react";
import { Transition } from "react-transition-group";

class PhotoSlide extends Component {
  constructor(props) {
    super(props);
    this.state = { entered: true };
  }

  toggleEnter() {
    this.setState({ entered: !this.state.entered });
  }

  render() {
    let photoDatum = this.props.photoDatum;
    let media = null;

    if (photoDatum) {
      const extension = photoDatum.url.split(".")[
        photoDatum.url.split(".").length - 1
      ];
      if (extension === "mp4") {
        media = (
          <video
            className="photoSlide-media"
            src={photoDatum.url}
            autoPlay
            playsInline
            loop
            alt=""
          />
        );
      } else {
        media = (
          <div
            className="photoSlide-media"
            style={{ backgroundImage: `url('${photoDatum.url}')` }}
          ></div>
        );
      }
    } else {
      media = <i>loading</i>;
    }
    return (
      <Transition appear timeout={1} in={this.state.entered}>
        {(state) => {
          return (
            <div
              className={`photoSlide photoSlide-${state}`}
              style={{ zIndex: this.props.zIndex }}
            >
              {media}
            </div>
          );
        }}
      </Transition>
    );
  }
}

export default PhotoSlide;
