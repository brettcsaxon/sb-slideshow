import React, { Component } from "react";
import ReactDOM from "react-dom";
import PhotoSlide from "./PhotoSlide";
import ReactCSSTransitionGroup from "react-transition-group";
import Mousetrap from "mousetrap";
import superagent from "superagent";
import { Route } from "react-router-dom";
import Index from "./Index";
import Slides from "./Slides";

class App extends Component {
  render() {
    return (
      <div>
        <Route exact path="/:galleryId" component={Slides} />
      </div>
    );
  }
}

export default App;
