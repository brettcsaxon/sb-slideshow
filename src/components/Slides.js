import React, { Component } from "react";
import PhotoSlide from "./PhotoSlide";
import ReactCSSTransitionGroup from "react-transition-group";
import Mousetrap from "mousetrap";
import superagent from "superagent";
import Settings from "./Settings";

class Slides extends Component {
  constructor(props) {
    super(props);
    this.state = {
      photoIndicesOnScreen: [],
      galleryPhotos: [],
      slidingIntervalId: null,
      settingsOpen: true,
      settings: {
        transitionTime: 3000,
        backgroundColor: "#ffffff",
      },
    };
    this.nextPhotoIndex = this.nextPhotoIndex.bind(this);
    this.randomPhotoIndex = this.randomPhotoIndex.bind(this);
    this.setTimeoutSetting = this.setTimeoutSetting.bind(this);
  }

  componentDidMount() {
    Mousetrap.bind(["space"], this.togglePlay.bind(this));
    this.getPhotos();
  }

  componentWillUnmount() {
    clearInterval(this.state.slidingIntervalId);
  }

  setTimeoutSetting(time) {
    let newState = Object.assign({}, this.state);
    newState.settings.transitionTime = time;
    this.setState(newState, this.restartWithNewSettings.bind(this));
  }

  setBackgroundSetting(e) {
    const backgroundColor = e.target.value;
    let newState = Object.assign({}, this.state);
    newState.settings.backgroundColor = backgroundColor;
    this.setState(newState);
  }

  getPhotos() {
    superagent
      .post(
        "https://v4-api.smilebooth.com/api/v4/images/list-by-gallery-noauth"
      )
      .send({ galleryId: parseInt(this.props.match.params.galleryId) })
      .end((err, res) => {
        this.setState((prevState, props) => {
          const visiblePhotos = res.body.filter(
            (photo) => photo.hidden !== true
          );
          const randomStart = Math.floor(Math.random() * visiblePhotos.length);
          return {
            galleryPhotos: visiblePhotos,
            photoIndicesOnScreen: [randomStart],
          };
        }, this.startSliding.bind(this));
      });
  }

  restartWithNewSettings() {
    this.pause();
    this.startSliding();
  }

  togglePlay() {
    if (this.state.slidingIntervalId) this.pause();
    else this.startSliding();
  }

  pause() {
    clearInterval(this.state.slidingIntervalId);
    this.setState({ slidingIntervalId: null });
  }

  startSliding() {
    this.setState({
      slidingIntervalId: setInterval(() => {
        this.addPhotoIndex(this.nextPhotoIndex());
      }, this.state.settings.transitionTime),
    });
  }

  nextPhotoIndex() {
    let randomPhotoIndex = this.randomPhotoIndex();
    let tries = 0;
    while (
      this.state.photoIndicesOnScreen.length > 1 &&
      tries < 50 &&
      this.state.photoIndicesOnScreen[
        this.state.photoIndicesOnScreen.length - 1
      ] === randomPhotoIndex
    ) {
      randomPhotoIndex = this.randomPhotoIndex();
      tries++;
    }
    return randomPhotoIndex;
  }

  randomPhotoIndex() {
    return Math.floor(Math.random() * this.state.galleryPhotos.length);
  }

  addPhotoIndex(photoIndex) {
    let photoIndexToAdd = [photoIndex];
    if (this.state.photoIndicesOnScreen.length === 0)
      photoIndexToAdd = [photoIndex, this.randomPhotoIndex()];
    const photoIndicesOnScreen = [
      ...this.state.photoIndicesOnScreen,
      ...photoIndexToAdd,
    ];
    this.setState({ photoIndicesOnScreen });
    // delete last photo after transition
    setTimeout(() => {
      this.setState((prevState, props) => {
        let trimmedPhotoIndices = [...prevState.photoIndicesOnScreen];
        trimmedPhotoIndices.splice(0, 1);
        return { photoIndicesOnScreen: trimmedPhotoIndices };
      });
    }, this.state.settings.transitionTime);
  }

  toggleSettings() {
    this.setState({ settingsOpen: !this.state.settingsOpen });
  }

  render() {
    let displayIndicies = [...this.state.photoIndicesOnScreen];
    let onScreenSlides = displayIndicies
      .reverse()
      .map((photoIndex, i) => (
        <PhotoSlide
          key={`photo-slide-${i}`}
          photoDatum={this.state.galleryPhotos[photoIndex]}
          zIndex={displayIndicies.length - i}
        />
      ));
    let loader = null;
    let settings = null;
    if (this.state.settingsOpen)
      settings = (
        <Settings
          settings={this.state.settings}
          setTimeoutSetting={this.setTimeoutSetting}
          setBackgroundSetting={this.setBackgroundSetting.bind(this)}
        />
      );
    if (this.state.photoIndicesOnScreen.length === 0)
      loader = (
        <div className="loaderHolder">
          <div className="loader slideshowLoader"></div>
        </div>
      );
    return (
      <div className="app">
        {loader}
        <div
          className="slideshowHolder"
          style={{ backgroundColor: this.state.settings.backgroundColor }}
        >
          {onScreenSlides}
        </div>
        {settings}
      </div>
    );
  }
}

export default Slides;
