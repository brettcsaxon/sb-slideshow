import React, { Component } from 'react'
import Slider from 'react-rangeslider'

class Settings extends Component {
  render() {
    const timeFormatSeconds = this.props.settings.transitionTime / 1000.0
    return (
      <div className="slideshowSettings">
        <h2>Slideshow Settings</h2>
        <div className="settingGroup">
          <label>Slide Interval: {timeFormatSeconds} seconds</label>
          <Slider min={1000} max={8000} step={250} value={this.props.settings.transitionTime} onChange={this.props.setTimeoutSetting} tooltip={false} />
        </div>
        <div className="settingGroup">
          <label>Background Color: </label>
          <input type="color" onChange={this.props.setBackgroundSetting} value={this.props.settings.backgroundColor} />
        </div>
      </div>
    )
  }
}

export default Settings
