This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

Smilebooth Gallery v2

## After cloning down...

### Run

```bash
npm install
```

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000/1069](http://localhost:3000/1069) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

# Smilebooth Gallery

React application with server side app for server rendering of some things.

## Production

The application is run on Heroku, and the production URL lives at [display.smilebooth.com/{id}](https://display.smilebooth.com/3) where ID is the ID of the gallery to display.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).
